from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import views

import jwt,json
from django.contrib.auth.models import User 

from user.api.serializers import RegisterSerializer
from rest_framework.authtoken.models import Token

@api_view(['POST'])
def registration_view(request):
    if request.method=='POST':
        serializer=RegisterSerializer(data=request.data)
        if serializer.is_valid():
            user=serializer.save()
            data={
                'response':'register success',
                'email':user.email,
                'username':user.username,
                'token':Token.objects.get(user=user).key
            }
        else:
            data=serializer.errors
        return Response(data)


class Login(views.APIView):
    template_name='user/login.html'
    def post(self,request,*args,**kwargs):
        if not request.data:
            return Response({'error':'please provide username and password',status:'400'})
        username=request.data['username']
        password=request.data['password']

        try:
            user=User.objects.get(username=username,password=password)
        except User.DoesNotExist:
            return Response({'Error':'Invalid username/password',status:"400"})
        
        if user:
            payload={
                'id':user.id,
                'email':user.email
            }
            jwt_token={'token':jwt.encode(payload,'SECRET_KEY')}
            return Response(json.dump(jwt_token),status=200,content_type='application/json')
        else:
            return Response(json.dump({'error':'Invalid credential'}),status=400,content_type='application/json')
