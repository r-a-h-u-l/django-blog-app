# <h1 align="center">Django Blog App</h1>
 
<!-- ABOUT THE PROJECT -->
## About The Project
 
Blog application to create simple blog. api's for all feature is made using Django rest framework
Features:
1. can register and login
2. can forget the password if user lost their password
3. can create, update, read and delete the blog post
 
### Built With
 
* [Python](https://www.python.org/)
* [Django](https://www.djangoproject.com/)
* [Django Rest Framework](https://www.django-rest-framework.org/)
 
### Installation
 
1. Download or clone the Repository to your device
2. Create Virtual Environment using `python3 -m venv <your environment name>`
3. Activate Virtual Environment for Mac and Linux user `source <virtual env path>/bin/activate`
4. Activate Virtual Environment for Windows user `venv\Scripts\activate`
5. type `pip install -r requirements.txt` (this will install required package for project)
6. type `python3 manage.py makemigrations`
7. type `python3 manage.py migrate`
8. type `python3 manage.py runserver`

### Project Snap

#### Home Page
![Screenshot_2020-12-24_at_10.11.47_AM](/uploads/a1d6864ca6f75bb3d5e89165903f4a1e/Screenshot_2020-12-24_at_10.11.47_AM.png)

#### Login and Register page
![Screenshot_2020-12-24_at_10.14.52_AM](/uploads/87b91094b02ffd7959b8815592efa387/Screenshot_2020-12-24_at_10.14.52_AM.png)

#### Account and New Post form
![Screenshot_2020-12-24_at_10.18.58_AM](/uploads/551ad61ba379dc826abb9bb3bbeab034/Screenshot_2020-12-24_at_10.18.58_AM.png)

#### Update and Delete view
![Screenshot_2020-12-24_at_10.25.28_AM](/uploads/b82dbdd2dae93fc3d0be890ff38dd936/Screenshot_2020-12-24_at_10.25.28_AM.png)
