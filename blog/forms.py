from .models import PostDraft
from django import forms


class PostDraftFrom(forms.ModelForm):
    class Meta:
        model = PostDraft
        fields = ["title", "content"]
