from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, JsonResponse
from .models import Post, PostDraft
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
)
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.models import User
from .forms import PostDraftFrom

# Create your views here.


class PostListView(ListView):
    model = Post
    template_name = "blog/home.html"
    context_object_name = "posts"
    ordering = ["-date_posted"]
    paginate_by = 5


class UserPostListView(ListView):
    model = Post
    template_name = "blog/user_posts.html"
    context_object_name = "posts"
    paginate_by = 5

    def get_queryset(self):
        user = get_object_or_404(User, username=self.kwargs.get("username"))
        return Post.objects.filter(author=user).order_by("-date_posted")


class PostDetailView(DetailView):
    model = Post


def post_create(request):
    if request.method == "POST":
        form = PostDraftFrom(request.POST)
        if form.is_valid():
            form = form.save(commit=False)
            title = form.title
            content = form.content
            Post.objects.create(title=title, content=content, author=request.user)
            if PostDraft.objects.filter(author=request.user):
                post_draft = PostDraft.objects.get(author=request.user)
                post_draft.delete()
            return redirect("blog-home")

    if request.method == "GET":
        if PostDraft.objects.filter(author=request.user):
            post_draft = PostDraft.objects.get(author=request.user)
            form = PostDraftFrom(instance=post_draft)
        else:
            form = PostDraftFrom()
        return render(request, "blog/post_form.html", {"form": form})


# class PostCreateView(LoginRequiredMixin, CreateView):
#     model = Post
#     fields = ["title", "content"]

#     def form_valid(self, form):
#         form.instance.author = self.request.user
#         if PostDraft.objects.filter(author=self.request.user):
#             post_draft = PostDraft.objects.get(author=self.request.user)
#             post_draft.delete()
#         return super().form_valid(form)

#     def get(self, request, *args, **kwargs):
#         if PostDraft.objects.filter(author=self.request.user):
#             post_draft = PostDraft.objects.get(author=self.request.user)
#             form = PostDraftFrom(instance=post_draft)
#         else:
#             form = PostDraftFrom()
#         return render(request, "blog/post_form.html", {"form": form})


class PostUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Post
    fields = ["title", "content"]

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author:
            return True
        return False


class PostDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Post
    success_url = "/"

    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author:
            return True
        return False


def save_draft(request):
    title = request.POST.get("title")
    content = request.POST.get("content")
    image = request.POST.get('image')
    get_draft_post = PostDraft.objects.filter(author=request.user)
    print(get_draft_post)
    if get_draft_post:
        draft_object = PostDraft.objects.get(author=request.user)
        draft_object.title = title
        draft_object.content = content
        draft_object.save()
    else:
        if title == "" and content == "":
            pass
        else:
            post_draft = PostDraft.objects.create(
                title=title, content=content, author=request.user
            )
    return JsonResponse({"test": "test"})


def about(request):
    return render(request, "blog/about.html", {"title": "About"})