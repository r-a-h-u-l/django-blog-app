from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.pagination import PageNumberPagination
from rest_framework.generics import ListAPIView
from rest_framework.authentication import TokenAuthentication
from rest_framework.filters import SearchFilter, OrderingFilter

from user.models import User
from blog.models import Post
from blog.api.serializers import BlogPostSerializer

@api_view(['GET'])
def api_blog_post_detail(request,pk):
    try:
        post=Post.objects.get(pk=pk)
    except post.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    
    if request.method=='GET':
        serializer=BlogPostSerializer(post)
        return Response(serializer.data)

@api_view(['PUT'])
@permission_classes((IsAuthenticated,))
def api_blog_post_update(request,pk):
    try:
        post=Post.objects.get(pk=pk)
    except post.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    user=request.user
    if post.author != user:
        return Response("You don't have permission to edit it.")

    if request.method=='PUT':
        serializer=BlogPostSerializer(post,data=request.data)
        if serializer.is_valid():
            serializer.save()
            data={'success':'update succes'}
            return Response(data=data)
        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)

@api_view(['DELETE'])
@permission_classes((IsAuthenticated,))
def api_blog_post_delete(request,pk):
    try:
        post=Post.objects.get(pk=pk)
    except post.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    
    user=request.user
    if post.author!=user:
        return Response("You don't have permission to delete it.")

    if request.method=='DELETE':
        operation=post.delete()
        if operation:
            data={'success':'delete success'}
        else:
            data={'failure':'delete failed'}
        return Response(data=data)

@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def api_blog_post_create(request):
    user=request.user
    post=Post(author=user)

    if request.method=='POST':
        serializer=BlogPostSerializer(post,data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)
    

class ApiBlogListView(ListAPIView):
    queryset=Post.objects.all()
    serializer_class=BlogPostSerializer
    authentication_classes=(TokenAuthentication,)
    permission_classes=(IsAuthenticated,)
    pagination_class=PageNumberPagination
    filter_backends=(SearchFilter,OrderingFilter)
    search_fields=('title','content','author__username')