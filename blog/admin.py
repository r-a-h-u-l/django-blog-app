from django.contrib import admin
from .models import Post, PostDraft

# Register your models here.
admin.site.register(Post)
admin.site.register(PostDraft)